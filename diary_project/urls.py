from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('', include('diary.urls')),
    path('admin/', admin.site.urls),
    # path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/', include('allauth.urls')),
    path('api/v1/', include('api.urls')),
    path('api-auth/', include('rest_framework.urls')),

    # path('admin/jsi18n', 'django.views.i18n.javascript_catalog', name='jsi18n'),

]
