from django.urls import path, include

from . import views
from .views import entry_search

# app_name = 'diary'

urlpatterns = [
    path('', views.HomepageView.as_view(), name='home'),
    path('entry_list/', views.EntryListView.as_view(), name='entry_list_all'),
    path('entry/<int:pk>/', views.EntryDetailView.as_view(), name='entry_detail'),
    path('entry/search/', entry_search, name='entry_search'),
    path('entry/new/', views.EntryCreateView.as_view(), name='entry_new'),
    path('document_list/', views.DocumentListView.as_view(), name='document_list_all'),
    path('document/<int:pk>/', views.DocumentDetailView.as_view(), name='document_detail'),
    path('document/new', views.DocumentCreateView.as_view(), name='document_new'),
    path('counterparty_list/', views.CounterpartyListView.as_view(), name='counterparty_list_all'),
    path('counterparty/<int:pk>/', views.CounterpartyDetailView.as_view(), name='counterparty_detail'),
    path('counterparty/new/', views.CounterpartyCreateView.as_view(), name='counterparty_new'),
    path('todo_list/', views.TodoListView.as_view(), name='todo_all'),
    path('todo/detail/<int:pk>/', views.TodoDetailView.as_view(), name='todo_detail'),
    path('todo/new/', views.TodoCreateView.as_view(), name='todo_new'),
    path('counterparty/all/', views.CounterpartyListView.as_view(), name='counterparty_all'),
    path('list_by_tag/<slug:tag_slug>/', views.entry_list, name='entry_list_by_tag'),
    path('todo/', views.TodoListView.as_view(), name='todolist'),
]
