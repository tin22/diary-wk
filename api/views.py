from rest_framework import generics
from rest_framework import viewsets

from diary import models
from . import serializers


class EntryViewSet(viewsets.ModelViewSet):
    """ To begin with, the first (more precisely, the last) 10 records
    """
    queryset = models.Entry.objects.order_by('-timestamp')[:10]
    serializer_class = serializers.EntrySerializer


class TodoListViewSet(viewsets.ModelViewSet):
    queryset = models.Todo.objects.order_by('-deadline')
    serializer_class = serializers.TodoListSerializer


# not used
class TodoListCreateAPIView(generics.ListCreateAPIView):
    queryset = models.Todo.objects.all()
    serializer_class = serializers.TodoListSerializer
