from rest_framework.routers import DefaultRouter

from .views import EntryViewSet, TodoListViewSet

router = DefaultRouter()
router.register('entries', EntryViewSet, basename='entries')
router.register('', TodoListViewSet)
urlpatterns = router.urls
