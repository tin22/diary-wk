from rest_framework import serializers
from diary.models import Entry, Todo


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'timestamp',
            'record',
            'status',
        )
        model = Entry


class TodoListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = (
            'id',
            'deadline',
            'action',
            'status',
        )
